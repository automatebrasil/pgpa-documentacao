# Instalação

## Configuração do Automate

Toda comunicação com com o Automate Enterprise é realizada pela RESTful API que por padrão de instalação vem desativada. A configuração desta API fica disponível na aba 'Options', opção 'Server settings' -> 'API Security'.

![](images/ConfiguracaoAPI1.png)

Na tela de configuração, marcar o checkbox 'Turn RESTful API on', mover o grupo 'Administrators' para 'Selected Group/User Name' e no painel inferior, marcar todas as opções em 'Allow'.

![](images/ConfiguracaoAPI2.png)

**Esta configuração deve ser feita obrigatoriamente antes da instalação do PGPA para que ocorra a comunicação entre os dois sistemas.**