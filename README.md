# Manuais

## PGPA 2.4

* [Manual de instalação](Manual 2.4/manual-pgpa-setup.md)
* [Manual do usuário](Manual 2.4/manual-user.md)
* [Manual do desenvolvedor](Manual 2.4/manual-dev.md)

## PGPA 2.3

* [Manual de instalação](Manual 2.3/manual-pgpa-setup.md)
* [Manual do usuário](Manual 2.3/manual-user.md)
* [Manual do desenvolvedor](Manual 2.3/manual-dev.md)

## Automate

* [Manual de instalação](Automate/setup.md)