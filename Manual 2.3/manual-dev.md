# PGPA 2.3 - Documentação Técnica

Este é o repositório de documentação e referências para a integração entre o Automate Enterprise e o PGPA (Portal de Governança de Processos Automatizados).

---

## Índice

### Requisitos e configurações

* [Task padrão](#markdown-header-task-padrao)
* [Constantes](#markdown-header-constantes)
* [Variáveis globais](#markdown-header-variaveis-globais)
* [Variáveis de ambiente do Windows](#markdown-header-variaveis-de-ambiente-do-windows)

### Chamadas

**Execução**

* [Log](#markdown-header-execucao-log)

**Usuários genéricos**

* [Obter usuário genérico](#markdown-header-obter-usuario-generico)
* [Bloquear usuário genérico](#markdown-header-bloquear-usuario-generico)
* [Gerar senha](#markdown-header-gerar-senha)

**Fila**

* [Inserir](#markdown-header-inserir)
* [Próximo](#markdown-header-proximo)
* [Atualizar](#markdown-header-atualizar)
* [Consultar](#markdown-header-consultar)

**Expurgo**

* [Expurgar](#markdown-header-expurgar)

---

# Requisitos e configurações

Toda comunicação entre os Workflows e o PGPA será realizada através de API REST, utilizando a Action HTTP Post, eliminando a necessidade de instruções SQL e chamadas no banco de dados.

## Task padrão
Antes de iniciar uma nova Task o desenvolvedor sempre deve baixar e importar o modelo padrão de acordo com a versão do Automate. Esta Task já contém o código de integração para iniciar e finalizar a execução no PGPA nos eventos OnTaskStart, OnTaskFailure e OnTaskEnd, a função Main e os demais eventos estão livres.

[Task base - Automate 10](./tasks/Task_Base_10.aml)

[Task base - Automate 11](./tasks/Task_Base_11.aml)

## Constantes

Para comunicação com o PGPA será necessário a criação de uma constante global no Automate com o nome **cPortalUrl** (Aba 'Options', opção 'Server Settings', 'Constants'). O valor desta constante é a URL do PGPA na rede interna da empresa, utilizando o nome ou o IP do servidor, por exemplo:

`http://server:9000/api`

`http://192.168.0.1:9000/api`


![](images/Constantes.jpg)


## Variáveis globais

Todas as variáveis globais (task variables) iniciadas com vPortal são utilizadas no controle da execução e não devem ter seus valores alterados durante a execução da Task.

## Variáveis de ambiente do Windows

As máquinas utilizadas para desenvolvimento de automações devem possuir uma variável de ambiente do Windows chamada **AUTOMATEDEV**. Quando esta variável é detectada, todos os logs de execuções serão direcionados para um Workflow especifico no PGPA, com objetivo de nunca enviar um log gerado durante o desenvolvimento para um Workflow de produção.

![](images/VariavelAmbiente.jpg)

---

# Execução

Abaixo o detalhamento das chamadas que poderão ser feitas pelo desenvolvedor no desenvolvimento da automação:

## Execução Log

Chamada utilizada para registrar o andamento dos passos executados na automação.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/execucao/log` (Fixo)

**Post type:** `Field` (Fixo)

| Nome                   | Descrição                                                              | Valor                   | Obrigatório |
| ---------------------- | ---------------------------------------------------------------------- | ----------------------- | ----------- |
| execucaoId             | (Fixo) Código da execução                                              | %vPortalExecucaoId%     | sim         |
| tarefa                 | (Fixo) Nome da Task                                                    | %GetTaskName()%         | não         |
| funcao                 | (Fixo) Nome da função                                                  | %GetFunctionName()%     | não         |
| linhaComando           | (Fixo) Linha da execução                                               | %GetCurrentLineNumber() | não         |
| statusId               | Código do status do log (tabela abaixo)                                |                         | sim         |
| identificacaoDocumento | Identifição de documento da iteração (exemplo: CNPJ, CPF, NF, CE, etc) |                         | não         |
| descricao              | Mensagem descritiva do que está sendo registrado                       |                         | sim         |

### Status

| Id  | Valor                                          |
| --- | ---------------------------------------------- |
| 2   | Mensagem em andamento                          |
| 5   | Iteração (loop registro) realizada com sucesso |
| 6   | Iteração (loop registro) realizada com erro    |

### AML

```
<AMHTTP ACTIVITY="post" AM_ONERROR="CONTINUE" IGNOREINVALIDCERTIFICATE="YES" TIMEOUT="2" URL="%vPortalUrl%/execucao/log" TYPE="field">
    <FIELD NAME="execucaoId" VALUE="%vPortalExecucaoId%" />
    <FIELD NAME="tarefa" VALUE="%GetTaskName()%" />
    <FIELD NAME="funcao" VALUE="%GetFunctionName()%" />
    <FIELD NAME="linhaComando" VALUE="%GetCurrentLineNumber()%" />
    <FIELD NAME="statusId" VALUE="" />
    <FIELD NAME="identificacaoDocumento" VALUE="" />
    <FIELD NAME="descricao" VALUE="" />
</AMHTTP>
```
### Exemplo

![](images/RequisicaoLog.jpg)


# Usuários genéricos

## Obter usuário genérico

Chamada utilizada para o obter dados de acesso de um determinado sistema.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/execucao/usuario/obter` (Fixo)

**Post type:** `Field` (Fixo)

| Nome       | Descrição                                   | Valor               | Obrigatório |
| ---------- | ------------------------------------------- | ------------------- | ----------- |
| execucaoId | (Fixo) Código da execução                   | %vPortalExecucaoId% | sim         |
| sistemaId  | Id numérico do sistema que será autenticado |                     | não         |

### Retorno

Retorna um JSON com os dados do usuário genérico. Sempre deve ser verificado se a propriedade 'autorizado' está com valor 'true', pois caso não houver usuário disponível para execução ou o sistema não estiver vinculado ao Workflow, os campos 'caminho', 'usuario' e 'senha' vão estar vazios.

```
{
   "id": 1,
   "acessoExclusivo": false,
   "autorizado":true,
   "caminho":"http://sistema.url",
   "usuario":"login",
   "senha":"senha",
   "mensagem":"Dados de acesso ao sistema ID 10 fornecidos a execução."
}
```

### AML

```
<AMHTTP ACTIVITY="post" AM_ONERROR="CONTINUE" TIMEOUT="2" URL="%vPortalUrl%/execucao/acesso" TYPE="field">
    <FIELD NAME="execucaoId" VALUE="%vPortalExecucaoId%" />
    <FIELD NAME="sistemaId" />
</AMHTTP>
```

### Exemplo

![](images/UsuarioGenericoObter.jpg)

---

## Bloquear usuário genérico

Chamada para bloquear/desativar um usuário genérico.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/execucao/usuario/desativar` (Fixo)

**Post type:** `Field` (Fixo)

| Nome      | Descrição            | Valor | Obrigatório |
| --------- | -------------------- | ----- | ----------- |
| usuarioId | (Fixo) Id do usuario |       | sim         |


## Gerar senha

Chamada para atualizar senha do usuário genérico

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/execucao/usuario/gerar_senha`

**Post type:** `Field` (Fixo)

| Nome        | Descrição            | Valor | Obrigatório |
| ----------- | -------------------- | ----- | ----------- |
| usuarioId   | Código do usuario    |       | sim         |
| senhaAntiga | Senha antiga         |       | sim         |
| senhaNova   | Senha nova           |       | sim         |
| expiracao   | Data de expiração    |       | não         |

### AML

```
<AMHTTP ACTIVITY="post" AM_ONERROR="CONTINUE" TIMEOUT="2" URL="%vPortalUrl%/execucao/usuario/gerar_senha" TYPE="field">
    <FIELD NAME="usuarioId" />
    <FIELD NAME="senhaAntiga" />
    <FIELD NAME="senhaNova" />
    <FIELD NAME="expiracao" />
</AMHTTP>
```

### Exemplo

![](images/GerarSenha.png)

---
# Fila

## Inserir

Chamada utilizada para inserir um novo item na fila.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/fila/inserir` (Fixo)

**Post type:** `Field` (Fixo)

**File field:** `arquivo` (Fixo)

**File path:** `C:\diretorio\arquivo.ext` (OPCIONAL: Deve ser informado apenas se for necessário o upload de arquivo vinculado ao item da fila.)


| Nome       | Descrição                     | Valor               | Obrigatório |
| ---------- | ----------------------------- | ------------------- | ----------- |
| workflowId | Id do workflow                | %vPortalWorkflowId% | Sim         |
| execucaoId | Id da execução                | %vPortalExecucaoId% | Não         |
| fila       | Nome da fila                  |                     | Sim         |
| referencia | Identificação do item na fila |                     | Sim         |
| conteudo   | Objeto JSON                   |                     | Sim         |

### Retorno

```
{
   "autorizado":true,
   "erro":"",
   "item":{
      "id":2,
      "workflowId":"1",
      "statusId":0,
      "execucaoId":null,
      "nome":"Processar",
      "referencia":"003",
      "conteudo":"{\"item\":1, \"value\":\"teste\"}",
      "arquivo":"2084dc58-e6f3-4f38-a8dc-a2018f3205db.txt",
      "mensagem":"Item retornando para a fila",
      "dataInclusao":"2018-03-14T14:39:40.127",
      "dataUltimoStatus":"2018-03-14T15:56:10.52",
      "dataInicioExecucao":null
   }
}
```

### AML

```
<AMHTTP ACTIVITY="post" SSLVERSION="Tat" TIMEOUT="10" URL="%vPortalUrl%/fila/inserir" TYPE="field" FILEFIELD="arquivo" DATA="">
   <FIELD NAME="workflowId" VALUE="%vPortalWorkflowId%" />
   <FIELD NAME="execucaoId" VALUE="%vPortalExecucaoId%" />
   <FIELD NAME="fila" />
   <FIELD NAME="referencia" />
   <FIELD NAME="conteudo" />
</AMHTTP>
```

## Próximo

Chamada utilizada para retornar o próximo item da fila. Após retornado, o item terá o status atualizado para 'Executando' até que seja feita a chamada de atualização.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/fila/proximo` (Fixo)

**Post type:** `Field` (Fixo)

| Nome       | Descrição      | Valor               | Obrigatório |
| ---------- | -------------- | ------------------- | ----------- |
| execucaoId | Id da execução | %vPortalExecucaoId% | Sim         |
| fila       | Nome da fila   |                     | Sim         |

### Retorno

```
{
   "autorizado":true,
   "erro":"",
   "item":{
      "id":1,
      "workflowId":"1",
      "statusId":0,
      "execucaoId":null,
      "nome":"Processar",
      "referencia":"001",
      "conteudo":"{\"item\":1, \"value\":\"teste\"}",
      "arquivo":"2cb6f914-7611-44b2-97ea-c42882accd5b.txt",
      "mensagem":"",
      "dataInclusao":"2018-03-14T14:38:05.263",
      "dataUltimoStatus":"2018-03-14T15:54:57.073"
   }
}
```

### AML

```
<AMHTTP ACTIVITY="post" SSLVERSION="Tat" TIMEOUT="10" URL="%vPortalUrl%/fila/proximo" TYPE="field">
   <FIELD NAME="execucaoId" VALUE="%vPortalExecucaoId%" />
   <FIELD NAME="fila" VALUE="" />
</AMHTTP>
```

## Atualizar

Chamado utilizada para atualizar o status do item da fila após o processamento.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/fila/atualizar` (Fixo)

**Post type:** `Field` (Fixo)

| Nome       | Descrição                                                                      | Valor               | Obrigatório |
| ---------- | ------------------------------------------------------------------------------ | ------------------- | ----------- |
| execucaoId | Id da execução                                                                 | %vPortalExecucaoId% | Sim         |
| filaId     | Identificador do item na fila                                                  |                     | Sim         |
| statusId   | Id do status do item a ser atualizado                                          |                     | Sim         |
| mensagem   | Campo livre para descrição de erro ou sucesso do processamento do item da fila |                     | Não         |

### Status

| Id  | Valor                 |
| --- | --------------------- |
| 0   | Pendente              |
| 1   | Executando            |
| 2   | Executado com sucesso |
| 3   | Executado com erro    |

### Retorno

```
{
   "autorizado":true,
   "erro":"",
   "item":{
      "id":2,
      "workflowId":"1",
      "statusId":0,
      "execucaoId":null,
      "nome":"Processar",
      "referencia":"003",
      "conteudo":"{\"item\":1, \"value\":\"teste\"}",
      "arquivo":"2084dc58-e6f3-4f38-a8dc-a2018f3205db.txt",
      "mensagem":"Item retornando para a fila",
      "dataInclusao":"2018-03-14T14:39:40.127",
      "dataUltimoStatus":"2018-03-14T15:56:10.52",
      "dataInicioExecucao":null
   }
}
```

### AML

```
<AMHTTP ACTIVITY="post" SSLVERSION="Tat" TIMEOUT="5" URL="%vPortalUrl%/fila/atualizar" TYPE="field">
   <FIELD NAME="execucaoId" VALUE="%vPortalExecucaoId%" />
   <FIELD NAME="filaId" VALUE="" />
   <FIELD NAME="statusId" VALUE="" />
   <FIELD NAME="mensagem" VALUE="" />
</AMHTTP>
```

## Consultar

Chamada utilizada para consultar se um determinado item existe na fila.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/fila/consultar` (Fixo)

**Post type:** `Field` (Fixo)

| Nome       | Descrição                     | Valor               | Obrigatório |
| ---------- | ----------------------------- | ------------------- | ----------- |
| workflowId | Id do workflow                | %vPortalWorkflowId% | Sim         |
| fila       | Nome da fila                  |                     | Sim         |
| referencia | Identificador do item na fila |                     | Sim         |

### Retorno

```
[
   {
      "id":2,
      "workflowId":"1",
      "statusId":0,
      "execucaoId":null,
      "nome":"Processar",
      "referencia":"003",
      "conteudo":"{\"item\":1, \"value\":\"teste\"}",
      "arquivo":"2084dc58-e6f3-4f38-a8dc-a2018f3205db.txt",
      "mensagem":"Item retornando para a fila",
      "dataInclusao":"2018-03-14T14:39:40.127",
      "dataUltimoStatus":"2018-03-14T15:56:10.52",
      "dataInicioExecucao":null
   },
   {
      "id":5,
      "workflowId":"1",
      "statusId":0,
      "execucaoId":null,
      "nome":"Processar",
      "referencia":"003",
      "conteudo":"{\"item\":1, \"value\":\"teste\"}",
      "arquivo":"8630adec-394a-43b2-98c4-a0773bba4fe5.txt",
      "mensagem":null,
      "dataInclusao":"2018-03-14T17:01:10.137",
      "dataUltimoStatus":"2018-03-14T17:01:10.137",
      "dataInicioExecucao":null
   }
]
```

### AML

```
<AMHTTP ACTIVITY="post" SSLVERSION="Tat" TIMEOUT="5" URL="%vPortalUrl%/fila/consultar" TYPE="field">
   <FIELD NAME="workflowId" VALUE="%vPortalWorkflowId%" />
   <FIELD NAME="fila" VALUE="" />
   <FIELD NAME="referencia" VALUE="" />
</AMHTTP>
```

## Expurgar

Esta chamada expurga a tabela de Auditoria. No Portal é possível configurar o número de dias para efetivar o expurgo.

### Parâmetros

**Action:** `HTTP (Post)`

**URL:** `%vPortalUrl%/expurgo`

### AML

```
<AMHTTP ACTIVITY="post" AM_ONERROR="CONTINUE" TIMEOUT="2" URL="%vPortalUrl%/expurgo">    
</AMHTTP>
```

---

# Downloads

[Task base - Automate 10](./tasks/Task_Base_10.aml)

[Task base - Automate 11](./tasks/Task_Base_11.aml)
