## Instalação do PGPA

Execute o instalador e confirme o diálogo de confirmação do Windows para executar o instalador como administrador.

Caso seja a primeira vez que a aplicação está sendo instalada, ela irá requerer a pasta de instalação:

### Define da pasta de instalação
![](images/pgpa-setup/1.0_AskFolder.png)

### Confirma pasta de instalação
![](images/pgpa-setup/1.1_AskFolder_ReadyToInstall.png)

No caso de atualizações, caso a aplicação já esteja instalada, era irá apenas pedir para prosseguir a instalação:

### Inicia instalação
![](images/pgpa-setup/1_ReadyToInstall.png)

A instalação tentará encerrar o serviço do Automate PGPA caso o mesmo esteja em execução.

### Encerra serviço do Automate PGPA
![](images/pgpa-setup/2_CloseService.png)

### Concluí a instalação
![](images/pgpa-setup/3_Finish.png)

Caso seja a primeira vez que a instalação foi executada, crie o arquivo appsetting.json na pasta raiz da instalação (exemplo: c:\Program Files\AutoMate Brasil\PGPA) com o seguinte conteúdo:

Exemplo: * [appsettings.json](appsettings.json)