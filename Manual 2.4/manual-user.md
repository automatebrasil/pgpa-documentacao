# PGPA - Manual do Usuário
## PORTAL DE GOVERNANÇA DE PROCESSOS AUTOMATIZADOS

Última atualização: 19/04/2018

# Sumário

- [1	Módulos principais](#markdown-modulos-principais)
- [2	Perfil de Usuário](#markdown-perfil-de-usuario)
- [2.1	Permissões de acesso](#markdown-permissoes-de-acesso)
- [3	Home](#markdown-home)
- [3.1.1	Workflows disponíveis](#markdown-workflows-disponiveis)
- [4	Monitoramento](#markdown-monitoramento)
- [5	Chamados](#markdown-chamado)
- [5.1	Dashboard](#markdown-dashboard)
- [5.2	Novo](#markdown-novo)
- [6	Cadastros](#markdown-cadastros)
- [6.1	Usuários](#markdown-usuarios)
- [6.2	Workflows](#markdown-workflows)
- [6.2.1	Versões](#markdown-versoes)
- [6.2.2	Status](#markdown-status)
- [6.2.3	Parâmetros](#markdown-parametros)
- [6.2.4	Sistemas](#markdown-sistemas)
- [6.2.5	Permissões](#markdown-permissoes)
- [7	Detalhes do Workflow](#markdown-detalhes-do-workflow)
- [7.1	Filas](#markdown-filas)
- [7.2	Histórico de Execuções](#markdown-historico-de-execucoes)
- [7.2.1	Detalhes do histórico de execuções](#markdown-detalhes-do-historico-de-execucoes)
- [8	Auditoria](#markdown-auditoria) 

# 1	Módulos principais
*	Home
*	Monitoramento
*	Chamados
*	Cadastros
*	Auditoria
# 2	Perfil de Usuário
O Portal dispõe de três perfis de usuário pré-definidos, que dão acesso a diferentes módulos / áreas do sistema:
*	Operador
*	Técnico
*	Administrador
## 2.1	Permissões de acesso

As permissões de acesso por perfil de usuário têm as seguintes atribuições:
Permissão de acesso por funcionalidade

![](images/Manual/permissoes.png)


# 3	Home
A página Home é acessível a todos os perfis de usuário. Nela, são exibidos os workflows disponíveis. 
### 3.1	Workflows disponíveis
Cada usuário pode ver os workflows disponíveis, ou seja, os workflows vinculados a sua permissão.
Cada item da lista exibe as seguintes informações:
*	Nome do Workflow, Centro de Custo, Versão e Dashboard
O dashboard exibe uma informação geral do estado da fila no dia de hoje. Cada ícone representa a contagem de itens nos estados: Aguardando, Em execução,  Encerrado com sucesso e Encerrado com falha, respectivamente.
Cada item da lista permite realizar as seguintes ações:
Visualizar workflow: exibe detalhes do workflow.

Executar workflow: executa o workflow.
Perfil: Administrador e operador.
# 4	Monitoramento
O monitoramento é acessível somente aos perfis Administrador e Técnico.
Esta página traz as seguintes informações da máquina que hospeda o servidor Automate:
*	Uso de Cpu
*	Uso de memória RAM
*	Quantidade de agentes
*	Tempo online
Exibe também:
*	Painel com os Workflows
*	Execuções em andamento
*	Execuções agendadas
*	Agentes
O painel de workflows permite que determinado workflow seja acessado, exibindo assim os detalhes do workflow.
As execuções em andamento permitem que uma execução seja interrompida. O único usuário com direito a interromper uma execução é o administrador.
# 5	Chamados
Utilizado pelo usuário para abrir um chamado (uma requisição), geralmente direcionada a uma equipe de sustentação, para resolver um possível problema com o robô ou para requerer melhorias / sugestões.
## 5.1	Dashboard
Exibe os chamados em vigor.
## 5.2	Novo
Permite a adição de um novo chamado.
Um novo chamado permite configurar os seguintes campos: 
*	Título, Hashtags, Workflow, Versão, Origem, Status, Usuários envolvidos, Detalhes: Criticidade, Andamento
### Origem: 
*	Email cliente
*	Telefone
*	Painel
*	Intervenção programada
*	Cliente
### Status:
*	Aberto
*	Aguardando
*	Serviço restabelecido
*	Causa raiz identificada/conhecida
*	Solução definitiva identificada
*	Finalizado
### Criticidade:
*	00 Backlog - tratar quando tiver tempo
*	01 Baixa - ocorre raramente e quando ocorre não é percebido pelo cliente
*	02 Baixa - ocorre com frequência, mas não e percebido pelo cliente.
*	03 Média - ocorre raramente, mas o cliente percebe e é afetado.
*	04 Média - ocorre com certa frequência e o cliente percebe e é afetado.
*	05 Alta - ocorre mais de 3x por semana e o cliente é afetado.
*	06 Alta - o resultado do problema pode acarretar prejuízos financeiros ou de imagem do cliente.
*	07 Altíssima - o workflow está impossibilitado de executar.
### Andamento:
*	Workflow não finalizou a operação
*	Workflow em loop infinito
*	Workflow retornou um resultado incompleto / inesperado
*	Problemas no ambiente rede/memória/processador/disco/mapeamento
*	Problemas no Automate SMC/DEV/CAL
*	Problemas na interface de arquivos/sites/aplicações
*	Problemas com os parâmetros
*	Problemas com o banco de dados
*	Dúvida/pesquisa
*	Melhorias/Evolução
# 6	Cadastros
O módulo cadastro gerencia os seguintes recursos do Portal:
*	Sistemas
*	Usuários
*	Workflows
*	Permissões
# 6.1	Usuários
Cadastra usuários do Portal.
O cadastro de um Usuário no Portal, requer os seguintes campos (obrigatórios):
Nome, Perfil (Operador, Técnico, Administrador), Ativo, Permissão (grupos de workflow ou “workflows disponíveis”), Email, Telefone, Login, Nova Senha, Confirmar Senha.
# 6.2	Workflows
Cadastra workflows.
Realiza apenas o cadastro “simples” de um workflow na base de dados do Portal. Nesta etapa, o Workflow cadastrado ainda não tem nenhum vínculo com o Automate. As informações iniciais do cadastro de um Workflow é apenas seu nome e o Centro de Custo. 
Um workflow permite cadastrar várias funcionalidades, entre elas:
*	Versões
*	Status
*	Parâmetros
*	Sistemas
*	Permissões
### 6.2.1	Versões
O cadastro de versões auxilia os desenvolvedores a terem controle sobre cada versão lançada de um workflow.
Ao cadastrar uma versão é possível atribuir as seguintes informações: 
*	Versão
*	Descrição
*	Desenvolvedor
*	Empresa
*	Responsável
*	Status
*	Workflow
Com especial destaque ao Workflow, que é um arquivo de extensão .ampkg (exportado do Automate) importado para o portal. A operação de exportação e importação é feita de forma manual.
### 6.2.2	Status
Permite cadastrar status de log, facilitando a auditoria dos desenvolvedores. Todo workflow tem por padrão os seguintes status:
| Id	| Nome	| Erro |	Iteração |
|---|---|---|---|
|2 |	Mensagem de andamento	|Não	|Não|
| 8|	Iteração (loop registro) realizada com sucesso|	Sim	|Sim|
|9	|Iteração (loop registro) realizada com erro	|Sim	|Sim|

### 6.2.3	Parâmetros
Permite cadastrar parâmetros de entrada requeridos pela execução do workflow. Um workflow pode ter nenhum ou muitos parâmetros de entrada. Cada parâmetro de entrada é baseado em chave e valor ou um arquivo.
### 6.2.4	Sistemas
Cadastra sistemas de acesso externo. Os sistemas são usados pelo robô para ter acesso a qualquer tipo de sistema externo com ou sem credenciais de acesso. As credenciais de acesso são definidas no módulo Usuários Genéricos.
#### 6.2.4.1	Usuários Genéricos
O usuário genérico contém dados pertinentes a um usuário e suas credenciais para acessar qualquer sistema externo que exija credencial. Um ou mais usuários genéricos podem ser atribuídos ao mesmo sistema.
### 6.2.5	Permissões
As permissões permitem agrupar um conjunto de workflows. Cada permissão pode ser atribuída a um usuário. Dessa forma, o usuário em questão terá acesso apenas aos workflows vinculados à sua permissão.
*O perfil administrador pode acessar todos os workflows independentemente da permissão atribuída a ele.
# 7	Detalhes do Workflow
Os detalhes de um workflow podem ser acessados pelas rotas de Home e também de Monitoramento.
Itens presentes nos detalhes do Workflow: Filas e Histórico de Execuções.
## 7.1	Filas
As filas é uma forma de gerenciar a ordem de execução dos Workflows.
Elas são exibidas na ordem crescente das colunas Status e Data último status, respectivamente.
### 7.1.1	Ações da fila: 
*	Exibir: exibe detalhes da fila
*	Aumentar prioridade: envia o item para o início da fila
*	Diminuir prioridade: envia o item para o fim da fila
*	Realocar: realoca o item na fila (duplica o mesmo item com o status aguardando no início da fila) 
*	Remover: remove o item da fila (seta seu status para removido)
### 7.1.2	Detalhes da fila:
*	Exibe o conteúdo da fila, que pode ser uma string ou json, entre outros dados como: Nome, Status, Data inclusão, Data último status, Data início execução, referência, arquivo, mensagem.
*	Permite fazer download do arquivo em anexo (caso exista).
*	Permite carregar log da fila (sob demanda, para evitar overload).
## 7.2	Histórico de Execuções
Exibe os workflows que foram executados ou estão em execução. O item de execução permite realizar duas ações: exibir detalhes da execução ou interromper a execução (caso esteja em execução). Esta última ação só pode ser realizada por um perfil de administrador.
### 7.2.1	Detalhes do histórico de execuções
Exibe um log completo da execução, imprescindível para acompanhar a execução do workflow, bem como identificar erros apontados pela execução (histórico de execuções).
# 8	Auditoria
A auditoria exibe um log completo de todas as ações, seja ela realizada no portal, seja realizada pelos robôs, exibindo as seguintes informações:
Id, Tabela, Usuário, Ação, Detalhes, Data.
Id e tabela representa uma informação do objeto ‘alterado’ na tabela x do banco de dados. O usuário é o usuário que executou a ação no portal. Caso a ação tenha sido executada no Automate, a palavra Console é exibida no campo Usuário. A ação é uma curta descrição da ação realizada, como cadastrar usuário, alterar parâmetro, remover sistema, executar workflow, interromper workflow, etc. Detalhes são informações adicionais significativas à interpretação da informação apresentada.
Somente o perfil administrador tem acesso a este módulo.