# Manuais

## PGPA

* [Manual de instalação](Manual/manual-pgpa-setup.md)
* [Manual do usuário](Manual/manual-user.md)
* [Manual do desenvolvedor](Manual/manual-dev.md)

## Automate

* [Manual de instalação](Manual/manual-automate-setup.md)